# REACH - Frontend Test - Android Apps

This is the code for the frontend test "Android Apps".

## Tech Stack

| <img src="https://angular.io/assets/images/logos/angular/angular.svg" width=32> | <img src="https://raw.githubusercontent.com/expressjs/expressjs.com/gh-pages/images/favicon.png" width=32> | <img src="https://avatars1.githubusercontent.com/u/22247014?v=3&s=400" width=32> |
| :---: | :---: | :---: |
| [Angular](https://angular.io/) | [Express](https://expressjs.com/)| [Yarn](https://yarnpkg.com/) |

## How to use this repository

This is a monorepo built with [Yarn](https://yarnpkg.com/). The packages of this test are in the `packages` folder.

To use a package, simply type `yarn workspace @reach/<package-name> <command>`.

For example, to start the server, you can type `yarn workspace @reach/server start`.

You will find some useful commands in the `package.json` of the root folder.

## First steps

In order to use this repo, you will need to install some software:

- [Node](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/)

After that, you need to install the repository dependencies with:

```yarn install```

Then, to use the api, you need to start the server with:

```yarn start:server```

and that will expose it at `localhost:3000` (you can change the port by setting the `API_PORT` environment to the desired port value. e.g.: `API_PORT=3001 yarn start:server`).

Finally, in another console, you can use the following command to start the client and start coding:

```yarn start:client```

## Monorepo

### Client

In the `packages/client` folder you will find the client. It contains the bare-bones of the app that you will build using [Angular](https://angular.io/) (v12).

### Interfaces

In the `packages/interfaces` folder you will find the types that are shared between the API and the client.

### Server

In the `packages/server` folder you will find the server. It is a very simple one built with [Express](https://expressjs.com/). The API is very simple and it is documented in the code.

## About the test

This is what you need to know about the test

## 🏁 Goals to achieve

Your goal is to develop the experience of searching for an app in android.

- The app must retrieve the apps from the server (`GET /apps`).

- The app must handle the server errors and act accordingly (e.g.: if there are no apps, seed them).

- The app must have a scrollable list (both for desktop and mobile).

- The app must sort the apps **alphabetically**.

- The app must have a reloading state similar to instagram. When you scroll up (moving your finger **down** the screen) while being at the top of the list, it must scroll the list a little bit down, showing a loader and perform a `POST /apps/seed` request to reload the apps (and then request them again).

- The app must have lateral navigation bar that has three features:

  - It must show in the primary color the initial of the first app in the top row.

  - If you click on a character, the list must scroll to the first row that has that initial.

  - If you start a click (or a tap) and move the cursor (or the finger) around, it should start scrolling, using the initial that is in the same vertical position as the mouse (or the finger) following the design (bluring the apps that don't have the requested initial, showing the initial in a bubble, etc).

- Try to copy [this design](https://www.figma.com/file/lUgeqQ1sREJawoK0FCUmhv/Frontend-test-apps-ui?node-id=0%3A1) (hosted in [Figma <img src="https://static.figma.com/app/icon/1/favicon.png" width=16 height=16>](https://www.figma.com/))

- The design must be <ins>**as precise as posible**</ins>.

- Always try to build it [as accessible as possible](https://developer.mozilla.org/en-US/docs/Web/Accessibility).

- You only need to edit the `packages/client` folder, but you are free to change as much as you want to build extra features or modify things that you don't like.

- You can add some content if you think it looks cool! Don't be shy.

- The code should be easily maintainable and easy to do some small modifications.

- The code should be easily readable.

### 🔨 What to use

- You can use external libraries.

- You can use your own libraries (if they are open-source, better).

- Basically, you can use everything that you want. Just use the right tool for the right job.

### <img src="https://thumbs.gfycat.com/GleefulParchedCirriped.webp" width=16 height=16> Extra work

- If you have time and want to earn some extra points, you can build as much extra features as you want.

### Keep in mind ⚠️

One of the goals is to <ins>**copy the design as precise as posible**</ins>, but that doesn't mean everything. For instance, if a modal has `700px` width, that is just a value for that screen. Be clever, <ins>**build the design responsive**</ins> to almost every screen (don't lose too much time with weird screens).

Furthermore, if you find something weird in the code but you don't have time to modify it, leave a comment! (mark it with a special flag like `FIXME`, `TODO`, etc).

### Other things

It would be cool if you could check the time you spend doing the test, so we know the difficulty of it. There are some tools that can help with that, I personally recommend [WakaTime](https://wakatime.com/) (it has a [VSCode extension](https://marketplace.visualstudio.com/items?itemName=WakaTime.vscode-wakatime)).

If you have any problem with the test, let us know!

Also, if you find something in the test that could be improved, we'd love to hear it!

---

Good luck and have fun! ❤️<img src="https://static.wixstatic.com/media/747ec2_160317ee585942458406206778bc35aa%7Emv2.png/v1/fill/w_32%2Ch_32%2Clg_1%2Cusm_0.66_1.00_0.01/747ec2_160317ee585942458406206778bc35aa%7Emv2.png" height=12 width=12>
