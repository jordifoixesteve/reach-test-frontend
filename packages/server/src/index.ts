import { MobileAppItem } from '@reach/interfaces';
import * as express from 'express';
import { random } from 'faker';
import * as cors from 'cors';

const app = express();
const port = process.env['API_PORT'] || 3000;

let mobileApps: MobileAppItem[] = [];
let seeding = false;
const MINIMUM_TIME_FOR_SEED_MS = 1000;
const MAXIMUM_TIME_FOR_SEED_MS = 4000;

const randomInt = (min: number, max: number): number => {
	return Math.floor(Math.random() * max) + min;
};

// ---------------------------------------------------------------------------------------------
// Necessary fix for development because of client & server being exposed in the localhost
app.use(
	cors({
		origin: '*',
	})
);
// ---------------------------------------------------------------------------------------------

/**
 * GET /apps
 *
 * This returns the apps that are in-memory at the time of the request.
 *
 * @returns 200 If the apps are ready to be served. The body contains the apps.
 * @returns 409 If the app is seeding the values.
 * @returns 404 If the app doesn't have the apps on memory.
 */
app.get(
	'/apps',
	(_req: express.Request, res: express.Response, _next: express.NextFunction) => {
		if (seeding) {
			res.status(409).send({ error: 'Seeding' });
		} else if (mobileApps.length === 0) {
			res.status(404).send({ error: 'There are no apps' });
		} else {
			res.status(200).send(mobileApps);
		}
	}
);

/**
 * POST /apps/seed
 *
 * This puts the apps in the server memory.
 *
 * Use this endpoint when you want to refresh the values or to seed it initially.
 *
 * There is a delay so the client can properly show the spinner.
 *
 * @returns 204 if the seed was succesful.
 * @returns 409 if a seed was already being performed.
 */
app.post(
	'/apps/seed',
	async (_req: express.Request, res: express.Response, _next: express.NextFunction) => {
		if (seeding) {
			res.status(409).send({ error: 'Already seeding' });
		} else {
			const start = Date.now();
			seeding = true;
			mobileApps = [];
			const appsLength = randomInt(40, 100);
			for (let i = 0; i < appsLength; i++) {
				const randomStr = `${Math.random()}`.substr(2);
				const hasImage = Math.random() > 0.33;
				const randomImage = hasImage
					? `https://picsum.photos/200/300?random=${randomStr}`
					: undefined;
				const randomWordsCount = Math.floor(Math.random() * 10) + 1;
				const randomName = random.words(randomWordsCount);

				mobileApps.push({
					name: randomName,
					imageUrl: randomImage,
				});
			}
			const finish = Date.now();

			const expectedTimeToSeed = randomInt(
				MINIMUM_TIME_FOR_SEED_MS,
				MAXIMUM_TIME_FOR_SEED_MS
			);
			const timeToWait = Math.max(0, expectedTimeToSeed - (finish - start));
			await new Promise<void>((res) => setTimeout(res, timeToWait));

			seeding = false;
			res.status(204).send();
		}
	}
);

/**
 * POST /apps/clear
 *
 * Removes the apps from the server memory.
 *
 * This will be used to test if the client can handle all the states.
 *
 * @returns 204 if the apps could be removed.
 * @returns 409 if a seed is being performed.
 */
app.post(
	'/apps/clear',
	(_req: express.Request, res: express.Response, _next: express.NextFunction) => {
		if (seeding) {
			res.status(409).send({ error: "Can't clear apps while seeding" });
		} else {
			mobileApps = [];
			res.status(204).send();
		}
	}
);

// start the Express server
app.listen(port, () => {
	console.log(`server started at http://localhost:${port}`);
});
