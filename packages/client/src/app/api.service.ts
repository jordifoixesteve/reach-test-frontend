import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, debounceTime, map, switchMap, tap } from 'rxjs/operators';
import { MobileAppItem } from '@reach/interfaces';
import { appStore } from './app.store';

@Injectable()
export class ApiService {
	constructor(private http: HttpClient) {}

	/**
	 * Runs a get request to retrieve the apps array from the server. If not successful, errors get treated as follows:
	 * - Error 404 (no apps found)
	 * 	 --> The server doesn't have a seed, so we post() to trigger its generation on the server.
	 * - Error 409 (apps not ready)
	 * 	 --> The server is busy, so we call this function again after a brief delay.
	 * @returns
	 */
	get(): Observable<any> {
		return this.http.get('/apps', { observe: 'response' }).pipe(
			catchError((error: HttpErrorResponse) => {
				if (error.status === 404) return of().pipe(switchMap((_) => this.post()));
				if (error.status === 409)
					return of().pipe(
						debounceTime(200),
						switchMap((_) => this.get())
					);
				throw error;
			}),
			map(
				(response: HttpResponse<MobileAppItem[]>) =>
					response.body?.sort((A, B) => {
						const a = A.name.toLowerCase();
						const b = B.name.toLowerCase();
						return a == b ? 0 : a > b ? 1 : -1;
					}) || []
			),
			tap((apps: MobileAppItem[]) => appStore.set('apps', apps))
		);
	}

	/**
	 * Runs a post request to seed the apps array in the server and returns them after generated. If not successful, errors get treated as follows:
	 * - Error 409 (seeding conflict)
	 * 	 --> The server is already seeding an array, so we run a get request to get them.
	 * @returns
	 */
	post(): Observable<any> {
		return this.http.post('/apps/seed', {}).pipe(
			catchError((error: HttpErrorResponse) => {
				if (error.status === 409) return of().pipe(switchMap((_) => this.get()));
				throw error;
			}),
			switchMap((_) => this.get())
		);
	}
}
