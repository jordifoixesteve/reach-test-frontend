import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppsViewComponent } from './apps-view/apps-view.component';
import { AppsFeedComponent } from './apps-feed/apps-feed.component';
import { ApiInterceptor } from './api.interceptor';
import { NgxSpinnerModule } from 'ngx-spinner';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AlphabeticalScrollBarModule } from 'alphabetical-scroll-bar';
import { ApiService } from './api.service';

@NgModule({
	declarations: [AppComponent, AppsViewComponent, AppsFeedComponent],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		HttpClientModule,
		NgxSpinnerModule,
		AlphabeticalScrollBarModule,
	],
	providers: [
		ApiService,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: ApiInterceptor,
			multi: true,
		},
	],
	bootstrap: [AppComponent],
	schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {}
