import { Injectable } from '@angular/core';
import {
	HttpEvent,
	HttpInterceptor,
	HttpHandler,
	HttpRequest,
	HttpHeaders,
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		const baseUrl = 'http://localhost:3000';
		const composedUrl = `${baseUrl}${req.url}`;
		const headers = new HttpHeaders({
			'Content-Type': 'application/json',
		});
		const apiReq = req.clone({ url: composedUrl, headers });
		HttpHeaders;
		return next.handle(apiReq);
	}
}
