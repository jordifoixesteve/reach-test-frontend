import {
	ChangeDetectionStrategy,
	Component,
	Input,
	HostListener,
	ViewChild,
	ElementRef,
} from '@angular/core';
import { MobileAppItem } from '@reach/interfaces';
import { AlphabeticalScrollBarComponent } from 'alphabetical-scroll-bar';
import { uniq } from 'lodash';
import { of } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { CHAR_SCROLLER_CHARS } from '../app.constants';

@Component({
	selector: 'apps-feed',
	templateUrl: './apps-feed.component.html',
	styleUrls: ['./apps-feed.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppsFeedComponent {
	@Input() set apps(value: MobileAppItem[]) {
		this._apps = value;
		this.usedLetters = uniq(value.map((app) => app.name[0].toUpperCase()));
	}
	@HostListener('touchmove', ['$event']) onTouchMove(event: any) {
		this.updateFingerTouch(event);
	}
	@HostListener('touchstart', ['$event']) onTouchStart(event: any) {
		this.updateFingerTouch(event);
	}
	@ViewChild('scrollBar') barComponent!: ElementRef<AlphabeticalScrollBarComponent>;
	_apps: MobileAppItem[] = [];
	currentLetter: string | null = null;
	currentValidLetter: string | null = null;
	allLetters: string[] = CHAR_SCROLLER_CHARS;
	usedLetters: string[] = [...this.allLetters];
	scrollEnabled = true;
	fingerY: number = 0;

	isAppActive(initial: string) {
		const index = this.allLetters.indexOf(initial.toUpperCase());
		if (index > -1)
			return this.currentValidLetter === this.allLetters[index].toLowerCase();
		return this.currentValidLetter === this.allLetters[0];
	}

	onLetterChange(event: any) {
		of((event as string).toLowerCase())
			.pipe(
				debounceTime(100),
				tap(() => {
					this.currentLetter =
						this.allLetters[(this.barComponent as any).letterNumber - 1];
				}),
				distinctUntilChanged()
			)
			.subscribe((letter: string) => {
				this.currentValidLetter = letter;
				const element = document.querySelector('.' + letter);
				if (element)
					this.scrollTo(element).finally(() => this.changeScrollEnabled(false));
			});
	}

	onBarTouchEnd(event: any) {
		this.currentLetter = null;
		this.currentValidLetter = null;
		this.changeScrollEnabled(true);
	}

	updateFingerTouch(touchEvent: any) {
		const touch = touchEvent.changedTouches[0];
		this.fingerY = touch.pageY - window.pageYOffset - 55;
	}

	changeScrollEnabled(value: boolean) {
		this.scrollEnabled = value;
	}

	async scrollTo(element: Element) {
		await element.scrollIntoView({
			behavior: 'smooth',
		});
	}
}
