import { BehaviorSubject, Observable } from 'rxjs';
import { MobileAppItem } from '@reach/interfaces';
import { APPS_STORE_DEFAULT_STATE } from './app.constants';
import { distinctUntilChanged, pluck } from 'rxjs/operators';
import cloneDeep from 'lodash-es/cloneDeep';

interface StoreState {
	apps: MobileAppItem[];
}

class AppStore {
	private readonly _subject: BehaviorSubject<StoreState>;
	private readonly _store: Observable<StoreState>;

	constructor(defaultState: StoreState) {
		this._subject = new BehaviorSubject<StoreState>({ ...defaultState });
		this._store = this._subject.asObservable();
	}

	select<K extends keyof StoreState>(name: K): Observable<StoreState[K]> {
		return this._store.pipe(pluck(name), distinctUntilChanged());
	}

	set<K extends keyof StoreState>(name: K, state: StoreState[K]): void {
		this._subject.next({ ...this._subject.getValue(), [name]: cloneDeep(state) });
	}
}

export const appStore = new AppStore(APPS_STORE_DEFAULT_STATE);
