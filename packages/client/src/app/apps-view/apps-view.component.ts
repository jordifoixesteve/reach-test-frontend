import {
	ChangeDetectionStrategy,
	ChangeDetectorRef,
	Component,
	ElementRef,
	HostListener,
	ViewChild,
} from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable, of, pipe } from 'rxjs';
import { MobileAppItem } from '@reach/interfaces';
import { ApiService } from '../api.service';
import { appStore } from '../app.store';

@Component({
	selector: 'apps-view',
	templateUrl: './apps-view.component.html',
	styleUrls: ['./apps-view.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppsViewComponent {
	/**
	 * Window scroll listener for non touch devices. It requests newly seeded apps if the user has scrolled up to the top of the page.
	 */
	@HostListener('window:scroll') onScroll = () => {
		if (!this.atAppsTop && window.pageYOffset === 0) {
			this.atAppsTop = true;
		} else if (window.pageYOffset > 55) {
			this.atAppsTop = false;
		}
	};
	@ViewChild('scrollFrame', { static: false }) scrollFrame?: ElementRef;
	@ViewChild('loader', { static: false }) loaderEl?: ElementRef;
	apps$: Observable<MobileAppItem[]> = appStore.select('apps');
	atAppsTop = false;
	autoScroll = false;
	requestOnTheWay = false;

	constructor(
		private apiService: ApiService,
		private spinner: NgxSpinnerService,
		private cd: ChangeDetectorRef
	) {}

	ngAfterViewInit() {
		setTimeout(async () => {
			await this.spinner.show();

			this.requestApps();
			const observer = new IntersectionObserver(
				async () => {
					if (!this.autoScroll && !this.requestOnTheWay) {
						await this.spinner.show();
						this.seedApps()?.add(() => this.cd.detectChanges());
					}
				},
				{
					root: null,
					rootMargin: '0px',
					threshold: 0.8,
				}
			);
			observer.observe(this.loaderEl!.nativeElement);
		});
	}

	requestApps() {
		this.requestOnTheWay = true;
		return this.apiService.get().subscribe(async (_) => {
			await this.spinner.hide();
			await this.scrollToAppsStart();
			this.requestOnTheWay = false;
			this.cd.detectChanges();
		});
	}

	seedApps() {
		if (this.requestOnTheWay) return;
		this.requestOnTheWay = true;
		return this.apiService.post().subscribe(async (_) => {
			await this.spinner.hide();
			await this.scrollToAppsStart();
			this.requestOnTheWay = false;
			this.cd.detectChanges();
		});
	}

	async scrollToAppsStart() {
		this.autoScroll = true;
		await this.scrollFrame?.nativeElement.scrollIntoView({
			behavior: 'smooth',
		});
		setTimeout(() => {
			this.autoScroll = false;
			this.atAppsTop = true;
		}, 300);
	}
}
