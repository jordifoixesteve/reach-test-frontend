export interface MobileAppItem {
	imageUrl?: string;
	name: string;
}
